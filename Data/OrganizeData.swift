//
//  OrganizeData.swift
//  reflect-demo-project
//
//  Created by Brenna Tirumalashetty on 3/28/20.
//  Copyright © 2020 BrennaTirumalashetty. All rights reserved.
//

import Foundation

class OrganizeData {
    
    static private let instance = OrganizeData()
    
    static func shared() -> OrganizeData {
        return self.instance
    }
    
        
    var events = [
        Event(label: "Saved Events", events: [
            EventStruct(eventTitle: "March at City Hall", eventTime: "11 am", eventDate: "4/04", eventDescription: "Come join us for a march at city hall to show your support for action on renewable energy in San Francisco. Come to the plaza in front of City Hall 30 minutes prior for more instructions.", eventLocation: "SF City Hall", organizer: "Organizer", contact: "sfreacts@gmail.com"),
            EventStruct(eventTitle: "Sit in", eventTime: "2 pm", eventDate: "4/15", eventDescription: "Come join our peaceful sit-in to protest Chase Bank's refusal to divest from the fossil fuel industry.", eventLocation: "Chase Bank", organizer: "Organizer", contact: "people@er.com")
        ]),
        Event(label: "Events Near You", events: [
            EventStruct(eventTitle: "How to reduce food waste", eventTime: "7:30 pm", eventDate: "5/04", eventDescription: "Come see the brilliant Michael Pollan give a talk on how to reduce food waste, in your own kitchen as well as through support of sustainable farming.", eventLocation: "Commonwealth Club SF", organizer: "Commonwealth Club", contact: "organizer@cwc.com"),
            EventStruct(eventTitle: "Climate Action Animation", eventTime: "5:30 pm", eventDate: "5/09", eventDescription: "Combine historical collage techniques with current digital media practice in this animation workshop. Using found objects and images of transportation, architecture, nature, and technology make a short video that inspires climate action.", eventLocation: "Space Arts Hub", organizer: "Green Art Workshop", contact: "org@gaw.com"),
            EventStruct(eventTitle: "TEDx Marin Online", eventTime: "7:30 pm", eventDate: "6/25", eventDescription: "This Program will discuss: How sea-level rise and extreme heat will play out in the Bay Area, new research on decarbonization solutions and economic impacts of various solutions including inaction, what the Government is planning or NOT, future energy technologies and where possible and quicker solutions may be found, and what human migration projections look like.", eventLocation: "Virtual", organizer: "TEDx Marin", contact: "events@TEDxMarin.com"),
            EventStruct(eventTitle: "Climate Hack 2020", eventTime: "9:00 am", eventDate: "7/15", eventDescription: "Our times may be uncertain, but one thing remains true - there’s a simple power in people coming together. That’s why KindSF and Starfish are launching Climate Hack 2020: Hack From Home.", eventLocation: "Starfish Mission", organizer: "Starfish Community", contact: "hello@climatehack.earth")
        ])
        
    ]
    
    var tempLabels = ["Date", "Time", "Location", "Organizer", "Contact"]
    var tempDetails = ["4/04", "11:00 am", "Civic Center Plaza, SF", "SF Climate Activists", "organizer@sfca@com"]
    
}

class Event {
    
    var label = ""
    var events = [EventStruct]()
    
    init(label: String, events: [EventStruct]) {
        self.label = label
        self.events = events
    }
}

struct EventStruct {
    var eventTitle = ""
    var eventTime = ""
    var eventDate = ""
    var eventDescription = ""
    var eventLocation = ""
    var organizer = ""
    var contact = ""
}
