//
//  TalkData.swift
//  reflect-demo-project
//
//  Created by Brenna Tirumalashetty on 3/28/20.
//  Copyright © 2020 BrennaTirumalashetty. All rights reserved.
//

import Foundation

class TalkData {
    
    static private let instance = TalkData()
    
    static func shared() -> TalkData {
        return self.instance
    }
    
    var issues = ["Transit", "Food", "Agriculture", "Clean Energy"]
    
    var messageTemplate = "Message Template"
    
    var people = ["Mark Haney", "Nancy Pelosi", "Gavin Newsom"]
    
    var searchablePeople = ["Brad Pitt", "Michael Mina", "Jerry Seinfeld", "Dame Judy Dench", "Elizabeth Warren", "Betsy DeVos", "Barack Obama", "Jose Andres", "Karen Bass", "Judy Chu", "Susan Davis", "Julia Brownley", "Ami Bera", "Pete Aguilar", "Ro Khanna", "Justin Amash", "Colin Allred"]
    
    var searchedRecipients = [String]()
    
    var recipients = [String]()
    
    var peopleList = [
        Person(label: "Mayor", officials: ["London Breed"]),
        Person(label: "City Supervisor", officials: ["Matt Haney"]),
        Person(label: "Governor", officials: ["Gavin Newsom"]),
        Person(label: "State Assembly", officials: ["David Chiu"]),
        Person(label: "State Senate", officials: ["Scott Wiener"]),
        Person(label: "Federal Congress Person", officials: ["Nancy Pelosi"]),
        Person(label: "Federal Senate", officials: ["Kamala Harris", "Dianne Feinstein"])
    ]
    
    var templatesList = [
        Template(title: "Public Transit", template: "Transit accounts for 29% of US greenhouse gas emissions. Public transit can reduce per person greenhouse gas emissions by 76% for train travel, 62% for light rail, and 33% for bus compared to individual transportation by car. Please support good public transportation in your city!"),
        Template(title: "Economy", template: "An economy based on continuous growth is unsustainable on a planet with finite resources. Support a new economy, support the Green New Deal!"),
        Template(title: "Agriculture", template: "Agriculture contributes 15-30% of global greenhouse gas emissions. Sustainable agriculture, with a focus on locally produced, plant-based products, can reduce the footprint of the agriculture industry by 50%. It's time to support our farmers in transitioning to a more sustainable model."),
        Template(title: "Renewable Energy", template: "Fossil fuels account for 62% of energy generated in the US. Replacing half of this with renewable energy sources by 2030 will give us a fighting chance at keeping temperature rise below 2 degrees celsius."),
        Template(title: "Air Travel", template: "Air travel causes 14% of global greenhouse emissions. It's time to hold the airlines to account for polluting our planet!")
    ]
    
}

class Person {
    
    var label = ""
    var officials = [String]()
    
    init(label: String, officials: [String]) {
        self.label = label
        self.officials = officials
    }
}

class Template {
    var title = ""
    var template = ""
    
    init(title: String, template: String) {
        self.title = title
        self.template = template
    }
}
