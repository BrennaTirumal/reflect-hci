//
//  TrackerData.swift
//  reflect-demo-project
//
//  Created by Brenna Tirumalashetty on 3/27/20.
//  Copyright © 2020 BrennaTirumalashetty. All rights reserved.
//

import Foundation

class TrackerData {
    
    static private let instance = TrackerData()
    
    static func shared() -> TrackerData {
        return self.instance
    }
    

    var householdItems = ["Shower", "Watch TV", "Laundry", "Dishes", "AC"]
    var travelItems = ["Bus", "Motorcycle", "Electric scooter", "Airplane"]
    
    //var dailyItems = ["Drive to work", "Hamburger and fries", "Watch TV"]
    //var dailyAmounts = ["1.0", "2.0", "0.5"]
    var itemSectionTitles = ["Saved Items", "All Items"]
    
//    var favoriteItems = ["Drive to Tahoe", "Cheese sandwich"]
//    var favoriteAmounts = ["3.0", "4.0"]
    
    var householdCo2e = [0.5, 0.1, 1.0, 0.75, 1.5]
    var travelCo2e = [0.3, 0.5, 0.1, 3.0]
    
    var foodsList = [
        ItemData(label: "Favorites", items: [item(name:"Peanut Butter", co2e: 0.3), item(name:"Black Beans", co2e: 0.5), item(name:"Sourdough Bread", co2e: 1.3), item(name:"Pasta", co2e: 0.6)]),
        ItemData(label: "All Foods", items: [item(name:"Butter", co2e: 2.0), item(name:"Chicken", co2e: 1.7)])
    ]
    
    var householdList = [
        ItemData(label: "Favorites", items: [item(name:"Watch TV", co2e: 0.3), item(name:"Laundry", co2e: 0.5), item(name:"Cooking", co2e: 1.3)]),
        ItemData(label: "All Household", items: [item(name:"Vacuum", co2e: 2.0), item(name:"Computer work", co2e: 1.7)])
    ]
    
    var travelList = [
        ItemData(label: "Favorites", items: [item(name:"Drive to work", co2e: 0.3), item(name:"Bus to work", co2e: 0.5), item(name:"Flight to NY", co2e: 1.3), item(name:"Drive to San Jose", co2e: 0.6)]),
        ItemData(label: "All Travel", items: [item(name:"Drive", co2e: 2.0), item(name:"Train", co2e: 1.7)])
    ]
    
    var dailyItemsList = [
        ItemData(label: "Food", items: [item(name:"Granola", co2e: 0.3), item(name:"Milk", co2e: 0.5), item(name:"Green Salad", co2e: 1.3)]),
        ItemData(label: "Travel", items: [item(name:"Drive to work", co2e: 0.3)]),
        ItemData(label: "Household", items: [item(name:"Laundry", co2e: 2.0), item(name:"Shower", co2e: 1.7)]),
        ItemData(label: "Other", items: [item(name:"Treadmill", co2e: 3.0)])
    ]
    
    var favoriteItemsList = [
        ItemData(label: "Food", items: [item(name:"Cheese Sandwich", co2e: 5.0)]),
        ItemData(label: "Travel", items: [item(name:"Drive to Tahoe", co2e: 150.0)]),
        ItemData(label: "Household", items: [item(name:"Watch TV", co2e: 15.0), item(name:"Shower", co2e: 1.7)])
    ]
    
    var foodSustainability = ["Organic?", "Local (source within 100 miles)?"]
    var householdSustainability = ["Hot water?", "Power save mode?"]
    var householdSustainabilityConst = [1.5, 0.75]
    
    var travelSustainability = ["Electric?", "Hybrid?", "Diesel?"]
    var travelSustainabilityConst = [0.5, 0.75, 1.5]
    
}

class ItemData {
        
    var label = ""
    var itemsList = [item]()
    
    init(label: String, items: [item]) {
        self.label = label
        self.itemsList = items
    }
    
}

struct item {
    var name = ""
    var co2e = 0.0
}
