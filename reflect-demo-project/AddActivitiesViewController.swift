//
//  AddActivitiesViewController.swift
//  reflect-demo-project
//
//  Created by Brenna Tirumalashetty on 3/28/20.
//  Copyright © 2020 BrennaTirumalashetty. All rights reserved.
//

import UIKit

class AddActivitiesViewController: UIViewController {
    
    @IBOutlet weak var favoritesTable: UITableView!
    var addedItem = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        favoritesTable.dataSource = self
        
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func addButton(_ sender: Any) {
        
        for indexPath in favoritesTable.indexPathsForSelectedRows ?? [] {
      
            TrackerData.shared().dailyItemsList[indexPath.section].itemsList.append(TrackerData.shared().favoriteItemsList[indexPath.section].itemsList[indexPath.row])
            
            addedItem = TrackerData.shared().favoriteItemsList[indexPath.section].itemsList[indexPath.row].name
        }
        
        let alert = UIAlertController(title: "Item Added!", message: "\(addedItem)", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
        NSLog("The \"OK\" alert occured.")
        }))
        self.present(alert, animated: true, completion: nil)
        
    }
    
}

extension AddActivitiesViewController: UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(TrackerData.shared().favoriteItemsList[section].itemsList.count)
        return TrackerData.shared().favoriteItemsList[section].itemsList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BasicCell", for: indexPath)
        
        let item = TrackerData.shared().favoriteItemsList[indexPath.section].itemsList[indexPath.row].name
        cell.textLabel?.text = item
        
        cell.detailTextLabel?.text = String(TrackerData.shared().favoriteItemsList[indexPath.section].itemsList[indexPath.row].co2e)
        
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return TrackerData.shared().favoriteItemsList.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return TrackerData.shared().favoriteItemsList[section].label
    }
    
}
