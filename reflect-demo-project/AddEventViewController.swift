//
//  AddEventViewController.swift
//  reflect-demo-project
//
//  Created by Brenna Tirumalashetty on 3/29/20.
//  Copyright © 2020 BrennaTirumalashetty. All rights reserved.
//

import UIKit

class AddEventViewController: UIViewController {
    
    var date = ""
    var time = ""
    
    @IBOutlet weak var titleField: UITextField!
    @IBOutlet weak var descriptionField: UITextView!
    @IBOutlet weak var organizerField: UITextField!
    @IBOutlet weak var contactField: UITextField!
    @IBOutlet weak var locationField: UITextField!
    @IBOutlet weak var datePickerField: UIDatePicker!
    
    @IBAction func datePicker(_ sender: Any) {
        let dateFormatter = DateFormatter()

        dateFormatter.dateStyle = DateFormatter.Style.short
        dateFormatter.timeStyle = DateFormatter.Style.short

        let strDate = dateFormatter.string(from: datePickerField.date)
        let dateTimeArray = strDate.components(separatedBy: ", ")
        
        date = dateTimeArray[0]
        time = dateTimeArray[1]
        
        print(strDate)
    }
    
    
    @IBAction func saveButton(_ sender: Any) {
        OrganizeData.shared().events[0].events.append(EventStruct(eventTitle: titleField.text!, eventTime: time, eventDate: date, eventDescription: descriptionField.text!, eventLocation: locationField.text!, organizer: organizerField.text!, contact: contactField.text!))
        
        let alert = UIAlertController(title: "Event Saved!", message: "'\(titleField.text!)'", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
        NSLog("The \"OK\" alert occured.")
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleField.delegate = self
        descriptionField.delegate = self
        organizerField.delegate = self
        locationField.delegate = self
        contactField.delegate = self
    }
}

extension AddEventViewController : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}

extension AddEventViewController : UITextViewDelegate {
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if (text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
        
    func textViewDidEndEditing(_ textView: UITextView) {
        let editedMessage = textView.text!
        print(editedMessage)
    }
    
}
