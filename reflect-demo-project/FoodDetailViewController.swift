//
//  FoodDetailViewController.swift
//  reflect-demo-project
//
//  Created by Brenna Tirumalashetty on 3/29/20.
//  Copyright © 2020 BrennaTirumalashetty. All rights reserved.
//

import UIKit

class FoodDetailViewController: UIViewController {
    
    var foodItemName = "Food Item"
    var foodItemCo2e = "0.0"
    
    @IBOutlet weak var foodDetailTableView: UITableView!
    
    @IBOutlet weak var quantityField: UITextField!
    
    @IBOutlet weak var totalCloudLabel: UILabel!
    
    @IBOutlet weak var foodItemLabel: UILabel!
    
    @IBAction func saveButton(_ sender: Any) {
        TrackerData.shared().dailyItemsList[0].itemsList.append(item(name: foodItemName, co2e: Double(totalCloudLabel.text!)!))
        
        let alert = UIAlertController(title: "Food Saved!", message: "'\(foodItemName)'", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
        NSLog("The \"OK\" alert occured.")
        }))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        foodItemLabel.text = "\(foodItemName) - \(foodItemCo2e) kg CO2e / g"
        foodDetailTableView.dataSource = self
        foodDetailTableView.delegate = self
        quantityField.delegate = self
        
    }
    

}

extension FoodDetailViewController : UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return TrackerData.shared().foodSustainability.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BasicCell", for: indexPath)
        
        let item = TrackerData.shared().foodSustainability[indexPath.row]
        cell.textLabel?.text = item
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        totalCloudLabel.text = String(format: "%.1f", Double(totalCloudLabel.text!)! * 0.9)
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        totalCloudLabel.text = String(format: "%.1f", Double(totalCloudLabel.text!)! / 0.9)
    }
    
}

extension FoodDetailViewController : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        let amount = Double(textField.text!)!
        let co2 = Double(foodItemCo2e)!
        totalCloudLabel.text = String(format: "%.1f", (co2 * amount))
    }
}
