//
//  Message2ViewController.swift
//  reflect-demo-project
//
//  Created by Brenna Tirumalashetty on 3/29/20.
//  Copyright © 2020 BrennaTirumalashetty. All rights reserved.
//

import UIKit

class Message2ViewController: UIViewController {
    
    var message = ""
    
    @IBOutlet weak var messageTextView: UITextView!
    
    @IBOutlet weak var recipientsTableView: UITableView!
    
    
    @IBAction func sendButton(_ sender: Any) {
        let alert = UIAlertController(title: "Message Sent!", message: "'\(message)'", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
        NSLog("The \"OK\" alert occured.")
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        recipientsTableView.dataSource = self
        messageTextView.delegate = self
        print(TalkData.shared().recipients)

        // Do any additional setup after loading the view.
    }
}

extension Message2ViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return TalkData.shared().recipients.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BasicCell", for: indexPath)
        
        let item = TalkData.shared().recipients[indexPath.row]
        cell.textLabel?.text = item
        
        return cell
    }
    
}

extension Message2ViewController : UITextViewDelegate {
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if (text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
        
    func textViewDidEndEditing(_ textView: UITextView) {
        message = textView.text!
        print(message)
    }
}

