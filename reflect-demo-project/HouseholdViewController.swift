//
//  HouseholdViewController.swift
//  reflect-demo-project
//
//  Created by Brenna Tirumalashetty on 4/26/20.
//  Copyright © 2020 BrennaTirumalashetty. All rights reserved.
//

import UIKit

class HouseholdViewController: UIViewController {

    
    @IBOutlet weak var savedHouseholdTable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        savedHouseholdTable.delegate = self
        savedHouseholdTable.dataSource = self
    }
}
    
extension HouseholdViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(TrackerData.shared().householdList[section].itemsList.count)
        return TrackerData.shared().householdList[section].itemsList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BasicCell", for: indexPath)
        
        let item = TrackerData.shared().householdList[indexPath.section].itemsList[indexPath.row].name
        cell.textLabel?.text = item
        
        cell.detailTextLabel?.text = String(TrackerData.shared().householdList[indexPath.section].itemsList[indexPath.row].co2e)
        
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return TrackerData.shared().householdList.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return TrackerData.shared().householdList[section].label
    }
            
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let cell = sender as? UITableViewCell, let indexPath = savedHouseholdTable.indexPath(for: cell), let householdDetailVC = segue.destination as? HouseholdDetailViewController {
            
            householdDetailVC.householdItemName = TrackerData.shared().householdList[indexPath.section].itemsList[indexPath.row].name
            householdDetailVC.householdItemCo2e = String(TrackerData.shared().householdList[indexPath.section].itemsList[indexPath.row].co2e)
        }
    }
    
}


