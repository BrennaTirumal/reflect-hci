//
//  TravelViewController.swift
//  reflect-demo-project
//
//  Created by Brenna Tirumalashetty on 5/3/20.
//  Copyright © 2020 BrennaTirumalashetty. All rights reserved.
//

import UIKit

class TravelViewController: UIViewController {

    @IBOutlet weak var travelTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        travelTableView.dataSource = self
        travelTableView.delegate = self

        // Do any additional setup after loading the view.
    }

}

extension TravelViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(TrackerData.shared().travelList[section].itemsList.count)
        return TrackerData.shared().travelList[section].itemsList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BasicCell", for: indexPath)
        
        let item = TrackerData.shared().travelList[indexPath.section].itemsList[indexPath.row].name
        cell.textLabel?.text = item
        
        cell.detailTextLabel?.text = String(TrackerData.shared().travelList[indexPath.section].itemsList[indexPath.row].co2e)
        
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return TrackerData.shared().travelList.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return TrackerData.shared().travelList[section].label
    }
            
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let cell = sender as? UITableViewCell, let indexPath = travelTableView.indexPath(for: cell), let travelDetailVC = segue.destination as? TravelDetailViewController {
            
            travelDetailVC.travelItemName = TrackerData.shared().travelList[indexPath.section].itemsList[indexPath.row].name
            travelDetailVC.travelItemCo2e = String(TrackerData.shared().travelList[indexPath.section].itemsList[indexPath.row].co2e)
        }
    }
    
}
