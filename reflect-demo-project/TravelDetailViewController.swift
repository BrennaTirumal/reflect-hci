//
//  TravelDetailViewController.swift
//  reflect-demo-project
//
//  Created by Brenna Tirumalashetty on 5/3/20.
//  Copyright © 2020 BrennaTirumalashetty. All rights reserved.
//

import UIKit

class TravelDetailViewController: UIViewController {
    
    var travelItemName = ""
    var travelItemCo2e = ""
    
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var distanceTextField: UITextField!
    @IBOutlet weak var sustainabilityTable: UITableView!
    @IBOutlet weak var cloudLabel: UILabel!
    
    @IBAction func saveButton(_ sender: Any) {
        TrackerData.shared().dailyItemsList[1].itemsList.append(item(name: travelItemName, co2e: Double(cloudLabel.text!)!))
        
        let alert = UIAlertController(title: "Activity Saved!", message: "'\(travelItemName)'", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
        NSLog("The \"OK\" alert occured.")
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleLabel.text = "\(travelItemName) - \(travelItemCo2e) kg CO2e / mile"
        sustainabilityTable.dataSource = self
        sustainabilityTable.delegate = self
        distanceTextField.delegate = self

        // Do any additional setup after loading the view.
    }
    
}

extension TravelDetailViewController : UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return TrackerData.shared().travelSustainability.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BasicCell", for: indexPath)
        
        let item = TrackerData.shared().travelSustainability[indexPath.row]
        cell.textLabel?.text = item
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        cloudLabel.text = String(format: "%.1f", Double(cloudLabel.text!)! * TrackerData.shared().travelSustainabilityConst[indexPath.row])
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        cloudLabel.text = String(format: "%.1f", Double(cloudLabel.text!)! / TrackerData.shared().travelSustainabilityConst[indexPath.row])
    }
    
}

extension TravelDetailViewController : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        let amount = Double(textField.text!)!
        let co2 = Double(travelItemCo2e)!
        cloudLabel.text = String(format: "%.1f", (co2 * amount))
    }
}

