//
//  OrganizeViewController.swift
//  reflect-demo-project
//
//  Created by Brenna Tirumalashetty on 3/25/20.
//  Copyright © 2020 BrennaTirumalashetty. All rights reserved.
//

import UIKit

class OrganizeViewController: UIViewController {

    @IBOutlet weak var eventsTableView: UITableView!
    
    override func viewWillAppear(_ animated: Bool) {
        eventsTableView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        eventsTableView.dataSource = self
        eventsTableView.delegate = self

        // Do any additional setup after loading the view.
    }
    
}

extension OrganizeViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return OrganizeData.shared().events[section].events.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BasicCell", for: indexPath)
        
        let item = OrganizeData.shared().events[indexPath.section].events[indexPath.row].eventTitle
        cell.textLabel?.text = item
        
        cell.detailTextLabel?.text = String(OrganizeData.shared().events[indexPath.section].events[indexPath.row].eventDate)
        
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return OrganizeData.shared().events.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return OrganizeData.shared().events[section].label
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let cell = sender as? UITableViewCell, let indexPath = eventsTableView.indexPath(for: cell), let eventDetailVC = segue.destination as? EventDetailViewController {
            
            eventDetailVC.eventName = OrganizeData.shared().events[indexPath.section].events[indexPath.row].eventTitle
            
            eventDetailVC.eventDescription = OrganizeData.shared().events[indexPath.section].events[indexPath.row].eventDescription
            
            eventDetailVC.details.append(OrganizeData.shared().events[indexPath.section].events[indexPath.row].eventDate)
            eventDetailVC.details.append(OrganizeData.shared().events[indexPath.section].events[indexPath.row].eventTime)
            eventDetailVC.details.append(OrganizeData.shared().events[indexPath.section].events[indexPath.row].eventLocation)
            eventDetailVC.details.append(OrganizeData.shared().events[indexPath.section].events[indexPath.row].organizer)
            eventDetailVC.details.append(OrganizeData.shared().events[indexPath.section].events[indexPath.row].contact)
            
        }
    }
    
}
