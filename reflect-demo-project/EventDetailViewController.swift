//
//  EventDetailViewController.swift
//  reflect-demo-project
//
//  Created by Brenna Tirumalashetty on 3/29/20.
//  Copyright © 2020 BrennaTirumalashetty. All rights reserved.
//

import UIKit

class EventDetailViewController: UIViewController {

    var eventName = "Event Name"
    var eventDescription = "Event Description"
    var labels = [String]()
    var details = [String]()
    var event = EventStruct()
    
    @IBOutlet weak var eventDetailTableView: UITableView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionField: UILabel!
    
    @IBAction func addButton(_ sender: Any) {
        
        event = EventStruct(eventTitle: eventName, eventTime: details[1], eventDate: details[0], eventDescription: eventDescription, eventLocation: details[2], organizer: details[3], contact: details[4])
        OrganizeData.shared().events[0].events.append(event)
        
        let alert = UIAlertController(title: "Event Saved!", message: "'\(eventName)'", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
        NSLog("The \"OK\" alert occured.")
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        eventDetailTableView.dataSource = self
        titleLabel.text = eventName
        descriptionField.text = eventDescription
        
    }
}

extension EventDetailViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return OrganizeData.shared().tempLabels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BasicCell", for: indexPath)
        
        let item = OrganizeData.shared().tempLabels[indexPath.row]
        cell.textLabel?.text = item
        
        cell.detailTextLabel?.text = details[indexPath.row]
        
        return cell
    }
    
}
