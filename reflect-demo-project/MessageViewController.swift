//
//  MessageViewController.swift
//  reflect-demo-project
//
//  Created by Brenna Tirumalashetty on 4/26/20.
//  Copyright © 2020 BrennaTirumalashetty. All rights reserved.
//

import UIKit

class MessageViewController: UIViewController {

    var pageTitle = "Default Title"
    var messageTemplate = "Default Message"
    var editedMessage = ""
    var selectedArray = [String]()
    
    var listOfNames = TalkData.shared().searchablePeople
    var filteredPeople = [String]()
    
    var searching = false
    
    @IBOutlet weak var templateTitle: UILabel!
    @IBOutlet weak var templateText: UITextView!
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    @IBOutlet weak var recipientsTable: UITableView!
    
    @IBOutlet weak var recipientsLabel: UILabel!
    
    @IBAction func sendButton(_ sender: Any) {
        let alert = UIAlertController(title: "Message Sent!", message: "'\(editedMessage)'", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
        NSLog("The \"OK\" alert occured.")
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func getRecipientsString() -> String {
        var recipients = ""
        
        for name in selectedArray {
            recipients = recipients + name + "\n"
        }
        
        print(recipients)
        
        return recipients
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        templateTitle.text = "\(pageTitle)"
        templateText.text = messageTemplate
        templateText.delegate = self
        recipientsTable.dataSource = self
        recipientsTable.delegate = self
        searchBar.delegate = self
        
        recipientsLabel.text = getRecipientsString()
        
    }
}

extension MessageViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searching {
            return filteredPeople.count
        }
        else {
            return TalkData.shared().searchablePeople.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BasicCell", for: indexPath)
        
        if searching {
            let item = filteredPeople[indexPath.row]
            cell.textLabel?.text = item
            cell.textLabel?.textColor = UIColor(cgColor: CGColor(srgbRed: 0.0, green: 0.0, blue: 1.0, alpha: 1.0))
        }
        
        else {
            let item = TalkData.shared().searchablePeople[indexPath.row]
            cell.textLabel?.text = item
            cell.textLabel?.textColor = UIColor(cgColor: CGColor(srgbRed: 0.0, green: 0.0, blue: 0.0, alpha: 0.0))
        }
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedName = filteredPeople[indexPath.row]
        if !selectedArray.contains(selectedName) {
            selectedArray.append(selectedName)
        }
        searching = false
        recipientsTable.reloadData()
        searchBar.text = ""
        recipientsLabel.text = getRecipientsString()
        print(selectedArray)
    }
    
}

extension MessageViewController : UITextViewDelegate {
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if (text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
        
    func textViewDidEndEditing(_ textView: UITextView) {
        editedMessage = textView.text!
        print(editedMessage)
    }
}

extension MessageViewController : UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filteredPeople = listOfNames.filter({$0.prefix(searchText.count) == searchText})
        searching = true
        recipientsTable.reloadData()
    }
    
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.endEditing(true)
        searchBar.resignFirstResponder()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        searching = false
        recipientsTable.reloadData()
    }
}

