//
//  PeopleViewController.swift
//  reflect-demo-project
//
//  Created by Brenna Tirumalashetty on 3/29/20.
//  Copyright © 2020 BrennaTirumalashetty. All rights reserved.
//

import UIKit

class PeopleViewController: UIViewController {

    
    @IBOutlet weak var peopleTableView: UITableView!
    
    @IBAction func goButton(_ sender: Any) {
        for indexPath in peopleTableView.indexPathsForSelectedRows ?? [] {
            TalkData.shared().recipients.append(TalkData.shared().peopleList[indexPath.section].officials[indexPath.row])
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        peopleTableView.dataSource = self

    }
    
}

extension PeopleViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return TalkData.shared().peopleList[section].officials.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BasicCell", for: indexPath)
        
        let item = TalkData.shared().peopleList[indexPath.section].officials[indexPath.row]
        cell.textLabel?.text = item
        
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return TalkData.shared().peopleList.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return TalkData.shared().peopleList[section].label
    }
    
}
