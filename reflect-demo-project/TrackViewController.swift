//
//  TrackViewController.swift
//  reflect-demo-project
//
//  Created by Brenna Tirumalashetty on 3/25/20.
//  Copyright © 2020 BrennaTirumalashetty. All rights reserved.
//

import UIKit
import Charts

private let chartHeight : CGFloat = 350.0


class TrackViewController: UIViewController {
    
    @IBOutlet weak var pieView: PieChartView!
    
    @IBOutlet weak var trackTableView: UITableView!
    
    
    @IBOutlet weak var dailyTotalLabel: UILabel!
    
    func calculateTotals() -> [Double]{
        let foodTotal = Double(String(format: "%.1f", getSumOfList(dataList: TrackerData.shared().dailyItemsList[0].itemsList)))!
        
        let travelTotal = Double(String(format: "%.1f", getSumOfList(dataList: TrackerData.shared().dailyItemsList[1].itemsList)))!
        
        let householdTotal = Double(String(format: "%.1f", getSumOfList(dataList: TrackerData.shared().dailyItemsList[2].itemsList)))!
        
        let otherTotal = Double(String(format: "%.1f", getSumOfList(dataList: TrackerData.shared().dailyItemsList[3].itemsList)))!
        
        let totalTotal = foodTotal + travelTotal + householdTotal + otherTotal
        
        return [foodTotal, travelTotal, householdTotal, otherTotal, totalTotal]
    }
    
    func calculatePieChartData() {
        let foodTotal = calculateTotals()[0]
        let travelTotal = calculateTotals()[1]
        let householdTotal = calculateTotals()[2]
        let otherTotal = calculateTotals()[3]
        
        var entries: [PieChartDataEntry] = Array()
        entries.append(PieChartDataEntry(value: foodTotal, label: "Food\n \(foodTotal)"))
        entries.append(PieChartDataEntry(value: travelTotal, label: "Travel\n  \(travelTotal)"))
        entries.append(PieChartDataEntry(value: householdTotal, label: "Household\n   \(householdTotal)"))
        entries.append(PieChartDataEntry(value: otherTotal, label: "Other\n \(otherTotal)"))
        
        let dataSet = PieChartDataSet(entries: entries, label: "")
        
        let c1 = NSUIColor(cgColor: CGColor(srgbRed: 0.5, green: 0.0, blue: 0.3, alpha: 1.0))
        let c2 = NSUIColor(cgColor: CGColor(srgbRed: 0.0, green: 0.2, blue: 0.5, alpha: 0.5))
        let c3 = NSUIColor(cgColor: CGColor(srgbRed: 0.3, green: 0.0, blue: 0.6, alpha: 1.0))
        let c4 = NSUIColor(cgColor: CGColor(srgbRed: 0.0, green: 0.1, blue: 0.7, alpha: 0.3))
        
        dataSet.colors = [c1, c2, c3, c4]
        dataSet.drawValuesEnabled = false
        
        pieView.data = PieChartData(dataSet: dataSet)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        trackTableView.reloadData()
        calculatePieChartData()
        
        dailyTotalLabel.text = String(format: "%.2f",calculateTotals()[4])
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //edgesForExtendedLayout = []
        //pieView.frame = CGRect(x: 0, y: 40, width: view.frame.size.width, height: chartHeight)
        
        //pieView.chartDescription?.enabled = true
        pieView.drawHoleEnabled = true
        pieView.rotationAngle = 0
        pieView.rotationEnabled = false
        pieView.isUserInteractionEnabled = false
        pieView.legend.enabled = false
        
        trackTableView.dataSource = self
        
    }
    
}

extension TrackViewController: UITableViewDataSource {
        
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(TrackerData.shared().dailyItemsList[section].itemsList.count)
        return TrackerData.shared().dailyItemsList[section].itemsList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BasicCell", for: indexPath)
        
        let item = TrackerData.shared().dailyItemsList[indexPath.section].itemsList[indexPath.row].name
        cell.textLabel?.text = item
        
        cell.detailTextLabel?.text = String(TrackerData.shared().dailyItemsList[indexPath.section].itemsList[indexPath.row].co2e)
        
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return TrackerData.shared().dailyItemsList.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return TrackerData.shared().dailyItemsList[section].label
    }
    
}

func getSumOfList(dataList: [item]) -> Double {
    var sum = 0.0
    for itemData in dataList {
        sum += itemData.co2e
    }
    return sum
}
