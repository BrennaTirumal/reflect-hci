//
//  IssuesViewController.swift
//  reflect-demo-project
//
//  Created by Brenna Tirumalashetty on 3/29/20.
//  Copyright © 2020 BrennaTirumalashetty. All rights reserved.
//

import UIKit

class IssuesViewController: UIViewController {

    @IBOutlet weak var issuesTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        issuesTableView.dataSource = self
        issuesTableView.delegate = self

        // Do any additional setup after loading the view.
    }
}

extension IssuesViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return TalkData.shared().templatesList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BasicCell", for: indexPath)
        
        let item = TalkData.shared().templatesList[indexPath.row]
        cell.textLabel?.text = item.title
        
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let cell = sender as? UITableViewCell, let indexPath = issuesTableView.indexPath(for: cell), let messageVC = segue.destination as? MessageViewController {
            
            messageVC.pageTitle = TalkData.shared().templatesList[indexPath.row].title
            messageVC.messageTemplate = TalkData.shared().templatesList[indexPath.row].template
        }
    }
    
}
