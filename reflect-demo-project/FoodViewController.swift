//
//  FoodViewController.swift
//  reflect-demo-project
//
//  Created by Brenna Tirumalashetty on 3/28/20.
//  Copyright © 2020 BrennaTirumalashetty. All rights reserved.
//

import UIKit

class FoodViewController: UIViewController {

    
    @IBOutlet weak var savedFoodsTable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        savedFoodsTable.dataSource = self
        savedFoodsTable.delegate = self
        

    }
}

extension FoodViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(TrackerData.shared().foodsList[section].itemsList.count)
        return TrackerData.shared().foodsList[section].itemsList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BasicCell", for: indexPath)
        
        let item = TrackerData.shared().foodsList[indexPath.section].itemsList[indexPath.row].name
        cell.textLabel?.text = item
        
        cell.detailTextLabel?.text = String(TrackerData.shared().foodsList[indexPath.section].itemsList[indexPath.row].co2e)
        
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return TrackerData.shared().foodsList.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return TrackerData.shared().foodsList[section].label
    }
            
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let cell = sender as? UITableViewCell, let indexPath = savedFoodsTable.indexPath(for: cell), let foodDetailVC = segue.destination as? FoodDetailViewController {
            
            foodDetailVC.foodItemName = TrackerData.shared().foodsList[indexPath.section].itemsList[indexPath.row].name
            foodDetailVC.foodItemCo2e = String(TrackerData.shared().foodsList[indexPath.section].itemsList[indexPath.row].co2e)
        }
    }
    
}
