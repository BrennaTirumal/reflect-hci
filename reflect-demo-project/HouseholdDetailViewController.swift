//
//  HouseholdDetailViewController.swift
//  reflect-demo-project
//
//  Created by Brenna Tirumalashetty on 4/26/20.
//  Copyright © 2020 BrennaTirumalashetty. All rights reserved.
//

import UIKit

class HouseholdDetailViewController: UIViewController {
    
    var householdItemName = "Household Item"
    var householdItemCo2e = "0.0"

    @IBOutlet weak var itemLabel: UILabel!
    
    @IBOutlet weak var timeField: UITextField!
    
    @IBOutlet weak var householdSustainabilityTable: UITableView!
    
    @IBOutlet weak var cloudLabel: UILabel!
    
    @IBAction func saveButton(_ sender: Any) {
        TrackerData.shared().dailyItemsList[2].itemsList.append(item(name: householdItemName, co2e: Double(cloudLabel.text!)!))
        
        let alert = UIAlertController(title: "Activity Saved!", message: "'\(householdItemName)'", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
        NSLog("The \"OK\" alert occured.")
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        itemLabel.text = "\(householdItemName) - \(householdItemCo2e) kg CO2e / min"
        householdSustainabilityTable.dataSource = self
        householdSustainabilityTable.delegate = self
        timeField.delegate = self
        // Do any additional setup after loading the view.
    }
    

}

extension HouseholdDetailViewController : UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return TrackerData.shared().householdSustainability.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BasicCell", for: indexPath)
        
        let item = TrackerData.shared().householdSustainability[indexPath.row]
        cell.textLabel?.text = item
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        cloudLabel.text = String(format: "%.1f", Double(cloudLabel.text!)! * TrackerData.shared().householdSustainabilityConst[indexPath.row])
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        cloudLabel.text = String(format: "%.1f", Double(cloudLabel.text!)! / TrackerData.shared().householdSustainabilityConst[indexPath.row])
    }
    
}

extension HouseholdDetailViewController : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        let amount = Double(textField.text!)!
        let co2 = Double(householdItemCo2e)!
        cloudLabel.text = String(format: "%.1f", (co2 * amount))
    }
}
